# Default template

## Preview

- [Example page](/res/adblockplus.org/screenshots/default-template.png)

## Contents

1. [Standard meta data](../includes/standard-meta-data.md)
1. [Social meta data](../includes/social-meta-data.md)
1. [Navbar](../includes/navbar.md)
1. [Heading](#heading)
1. [Table of contents](#table-of-contents)
1. Page body
1. [Footer](../includes/footer.md)

### Heading

The default template automatically inserts an h1 into the main content area containing it's page's `title` attribute unless it's page's `noheading` attribute is set truthy.

### Table of contents

The default template automatically inserts a table of contents into it's main content area if it's page's contents has headings with `id` attributes and it's page's `notoc` attribute is not set truthy.
